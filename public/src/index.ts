import { LoopCommerce as LC } from './core/LoopCommerce';

// declare global {
// 	interface Window {
// 		LoopCommerce?: LC | undefined;
// 	}
// }
window.LoopCommerce = new LC({ debug: true, id: 'f5633971-7c71-4a13-a780-febb631000c3' });
// const inst = new window.LoopCommerce({ debug: true });
window.LoopCommerce.initialize({ id: 'f5633971-7c71-4a13-a780-febb631000c3' });
