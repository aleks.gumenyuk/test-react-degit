import { API_ENDPOINT } from '../../consts';
import { createQueryString } from '../createQueryString/createQueryString';

export type FetchParams = RequestInit & {
	queryParams?: Record<string, string>;
};

export async function baseFetch<Res = any>(
	url: string,
	options?: FetchParams
): Promise<Res> {
	const _url = options?.queryParams
		? `${API_ENDPOINT}${url}?${createQueryString(options.queryParams)}`
		: `${API_ENDPOINT}${url}`;

	const reqBody = options?.body ? JSON.stringify(options.body) : undefined;

	const headers = {
		headers: {
			'Content-Type': 'application/json',
			...options?.headers,
		},
	};

	const _options = Object.assign(
		{
			method: 'GET',
			body: reqBody,
		},
		options,
		headers
	);

	try {
		const response = await fetch(_url, _options);
		return response.json();
	} catch (error) {
		console.error(error)
		throw new Error('An error occurred on fetching data');
	}
}
