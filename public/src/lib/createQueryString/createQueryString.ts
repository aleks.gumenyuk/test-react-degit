export function createQueryString(params: Record<string, string>) {
	const queryString = new URLSearchParams(params);
	return queryString.toString();
}
