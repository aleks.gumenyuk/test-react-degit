import { FetchParams, baseFetch } from '../baseFetch/baseFetch';

type TRequestMethods = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'OPTIONS' | 'PATCH';

export async function fetchData<Res = any>(
	url: string,
	method: TRequestMethods = 'GET',
	options?: FetchParams
): Promise<Res> {
	const _options = Object.assign(
		{
			method,
		},
		options
	);

	try {
		const response = await baseFetch(url, _options);
		return response;
	} catch (error) {
		console.error(error);
		throw new Error('An error occurred on fetching data');
	}
}
