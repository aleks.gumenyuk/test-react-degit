const welcomeMsgStyles = [
 "color: yellow",
 "font-size: 1.5rem",
 "font-weight: bold",
 "background-color: dodgerblue",
 "padding: 0.3rem 0.8rem",
 "margin: 1rem auto",
 "font-family: Consolas, Courier New, sans-serif",
 "border: 3px solid yellow",
 "border-radius: 4px",
 "text-shadow: 1px 1px 1px black",
]

const statusMsgTitleStyles = [
 "color: dodgerblue",
 "font-size: 1rem",
 "font-weight: medium",
//  "background-color: dodgerblue",
//  "padding: 0.3rem 0.8rem",
//  "margin: 1rem auto",
 "font-family: Consolas, Courier New, sans-serif",
//  "border: 3px solid yellow",
//  "border-radius: 4px",
//  "text-shadow: 1px 1px 1px black",
]

const statusErrorMsgTitleStyles = [
 "color: red",
 "font-size: 1rem",
 "font-weight: medium",
//  "background-color: dodgerblue",
//  "padding: 0.3rem 0.8rem",
//  "margin: 1rem auto",
 "font-family: Consolas, Courier New, sans-serif",
//  "border: 3px solid yellow",
//  "border-radius: 4px",
//  "text-shadow: 1px 1px 1px black",
]

export function welcomeMsg(title: string) {
 return console.log(
  `%c${title}`,
  welcomeMsgStyles.join(';'));
}

export function statusMsg(title:string, msg: string) {
  return console.log(
    `%c${title} \n%c${msg}`,
    statusMsgTitleStyles.join(';'),
    'color: yellow; background-color: black;'
  );
}
export function statusErrorMsg(title:string, msg: string) {
  return console.log(
    `%c${title} \n%c${msg}`,
    statusErrorMsgTitleStyles.join(';'),
    'color: yellow; background-color: black;'
  );
}
