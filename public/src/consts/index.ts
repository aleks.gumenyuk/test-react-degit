export const API_ENDPOINT = 'https://tracker-9hqt5.ondigitalocean.app/tracker-api/v1';
export const LOCAL_STORAGE_USER_ID = 'loop-commerce-user-id';

export const MESSENGERS_DEEP_LINK = {
  telegram: 'tg://',
  whatsapp: 'whatsapp://',
  viber: 'viber://',
  wa: 'whatsapp://',
  messenger: 'fb-messenger://',
  imessage: 'sms:',
  facetime: 'facetime://',
  line: 'line://',
  instagram: 'instagram://',
  fbMessenger: 'fb-messenger://',
  snapchat: 'snapchat://',
  skype: 'skype:', /* deep link not exist */
  twitter: 'twitter://',
  threema: 'threema://',
  wechat: 'weixin://',
}