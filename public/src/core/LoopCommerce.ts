import { LOCAL_STORAGE_USER_ID } from '../consts';
import {
	LocalStorage,
	createQueryString,
	fetchData,
	statusErrorMsg,
	statusMsg,
	welcomeMsg,
} from '../lib';
import { sendCurrentPageLocation } from '../services';

interface IInitTrackingRes {
	id: string;
	date: string;
}

interface LoopCommerceOptions {
	id: string;
	debug?: boolean;
}

export class LoopCommerce {
	params: LoopCommerceOptions | undefined;
	userId: string;
	pageLocation: string;
	trackingLinkElements: NodeListOf<HTMLAnchorElement> | null;
  initialized: boolean;

	constructor(params?: LoopCommerceOptions) {
		this.params = params;
		this.userId = '';
		this.trackingLinkElements = null;
		this.pageLocation = window.location.href;

		/**
		 * Every time the page url changes (SPA), it executes onPageLocationChange() method.
		 */
		// window.navigation.addEventListener('navigatesuccess', this.onPageLocationChange.bind(this));
		// window.addEventListener('locationchange', this.onPageLocationChange.bind(this));
		// window.addEventListener('pageshow', this.onPageLocationChange.bind(this));
		this.bodyMutationObserver();
		window.addEventListener('load', this.onLoad.bind(this));
	}

	private bodyMutationObserver() {
		const observer = new MutationObserver(() => {
			if (this.pageLocation !== window.location.href) {
				this.pageLocation = window.location.href;

				this.onPageLocationChange();
			}
		});

		const config = { subtree: true, childList: true };
		observer.observe(document.body, config);
	}
	/** Find all tracking links and set an id query parameter after the page load  */
	private onLoad() {
    console.log('loaded')
    this.selectTrackingLinks();
    this.addUserIdQueryToTrackingLinks();
	}
	/** Initializing the class */
	public async initialize({ id }: LoopCommerceOptions): Promise<void> {
		if (this.params?.debug) {
			welcomeMsg('LoopCommerce 🚀');
		}

		if (!this.checkIfUserHasId()) {
			await this.createUser(id);
		}

		sendCurrentPageLocation({
			location: this.pageLocation,
			userId: this.userId,
		});
    this.selectTrackingLinks();
    this.addUserIdQueryToTrackingLinks();
    this.initialized = true;
	}
	/**
	 * Check if a user already has user id in his local storage.
	 * @returns boolean
	 */
	private checkIfUserHasId() {
		const userId = LocalStorage.getItem(LOCAL_STORAGE_USER_ID) as string | null;
		if (userId) {
			this.userId = userId;
		}
		return !!userId;
	}
	/**
	 * Send request ot the server and get a user id.
	 * Save the user id to the local storage.
	 */
	private async createUser(id: string): Promise<void> {
		const response = await fetchData<IInitTrackingRes>(
			`/init-tracking/${id}/`,
			'GET',
			{
				queryParams: this.prepareQueryParams(),
			}
		);
    console.log('created')
		if ('id' in response) {
			this.userId = response.id;
			LocalStorage.setItem(LOCAL_STORAGE_USER_ID, response.id);
		}
	}
	/**
	 * Prepare a query object with the necessary data for the init tracking request.
	 * @returns Object
	 */
	private prepareQueryParams() {
		return {
			platform: window.navigator.platform /* DEPRECATED */,
			// oscpu: window.navigator.oscpu ?? '' /* DEPRECATED */,
			languages: window.navigator.languages.join(),
			timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
			resolution: `${window.screen.width}x${window.screen.height}`,
			// locations: this.appsThatUserHasOnDevice(),
		};
	}
	/**
	 * Find all tracking links and add the user id to them as a query parameter.
	 * We also send a request to know user's current page.
	 */
	private onPageLocationChange(): void {
		this.selectTrackingLinks();
		this.addUserIdQueryToTrackingLinks();

		const url = new URL(window.location.href);
		// const url = new URL(window.navigation.currentEntry.url);
		// const url = new URL(event.destination.url);

		if (this.params?.debug) {
			statusMsg(
				'onPageLocationChange() method ➡️',
				`Pathname changed to: ${url.href}`
			);
		}

		sendCurrentPageLocation({
			location: url.href,
			userId: this.userId,
		});
	}
	/**
	 * Add a user id as a query parameter to the all tracking links.
	 */
	private addUserIdQueryToTrackingLinks() {
		if (!this.trackingLinkElements?.length) {
			if (this.params?.debug) {
				statusErrorMsg(
					'addUserIdQueryToTrackingLinks() method ⛔',
					`There are no tracking links on the page`
				);
			}
			return;
		}

		this.trackingLinkElements.forEach((link) => {
			link.search = createQueryString({
				id: this.userId,
			});
		});
	}
	/**
	 * Find and appointment all tracking links with the attribute `[data-loop-commerce]`
	 * @returns All links with the attribute `[data-loop-commerce]`
	 */
	private selectTrackingLinks(): NodeListOf<HTMLAnchorElement> | null {
		return (this.trackingLinkElements =
			document.querySelectorAll<HTMLAnchorElement>('[data-loop-commerce]'));
	}
}
