import { fetchData } from '../../lib';

interface IRequest {
	userId: string;
	location: string;
}

export async function sendCurrentPageLocation({
	location,
	userId,
}: IRequest): Promise<any> {
	return await fetchData(`/tracking/${userId}/`, 'GET', {
		headers: { 'tracking-origin': location },
	});
}
