import {FC} from 'react';
import { Link } from 'react-router-dom';

const About: FC = function () {
  return (
    <>
    <h1>About page</h1>
    <Link data-loop-commerce to="https://imsg.link/opt-in/wF9jC/">Tracking Link</Link>
    </>
  );
};

export default About;
