import {About, Home} from '@pages';
import {HOME_PATH} from './name';
import {RouteObject, createBrowserRouter} from 'react-router-dom';
import {Layout} from '@components';

const routes: RouteObject[] = [
  {
    element: <Layout />,
    children: [
      {path: HOME_PATH, element: <Home />},
      {path: '/about', element: <About />},
    ],
  },
];

export default createBrowserRouter(routes);
