import {FC} from 'react';
import {Link, Outlet} from 'react-router-dom';

const Layout: FC = () => {
  return (
    <div>
      <div>
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
      </div>
      <div>
        <Outlet />
      </div>
    </div>
  );
};
export default Layout;
